import nltk
from nltk.corpus import stopwords
import joblib
import nltk 
import pandas as pd
import numpy as np
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize 
stop_words = set(stopwords.words('english'))
import preprocessor as p
from src.utils.wiki_article_regressor import *
from src.utils.helpper_function import *
import emoji
from sklearn.model_selection import cross_val_score, GridSearchCV, KFold, RandomizedSearchCV, train_test_split
p.set_options(p.OPT.MENTION, p.OPT.URL)
import pandas as pd
pd.set_option('display.max_colwidth', 100)

misspell_data = pd.read_csv("data/aspell.txt",sep=":",names=["correction","misspell"])
misspell_data.misspell = misspell_data.misspell.str.strip()
misspell_data.misspell = misspell_data.misspell.str.split(" ")
misspell_data = misspell_data.explode("misspell").reset_index(drop=True)
misspell_data.drop_duplicates("misspell",inplace=True)
miss_corr = dict(zip(misspell_data.misspell, misspell_data.correction))
def tokenize_text(text):
    tokens = []
    for sent in nltk.sent_tokenize(str(text)):
        if len(sent) < 2:
            continue
        for word in nltk.word_tokenize(sent):
            if word not in stop_words: 
                tokens.append(word.lower())
    return tokens
def misspelled_correction(val):
    for x in val.split(): 
        if x in miss_corr.keys(): 
            val = val.replace(x, miss_corr[x]) 
    return val
def cont_to_meaning(val): 
    for x in val.split(): 
        if x in cont_dic.keys(): 
            val = val.replace(x, cont_dic[x]) 
    return val
def punctuation(val): 
    punctuations = '''()-[]{};:'"\,<>./@#$%^&_~*.!!'''
    for x in val.lower(): 
        if x in punctuations: 
            val = val.replace(x, " ") 
    return val

def replace_special_punctua(val):
    val = val.replace('...', " ")
    val = val.replace('*', " ")
    val = val.replace('***', " ")
    val = val.replace('****', " ")
    val = val.replace('   ', " ")
    val = val.replace('  ', " ")
    val = val.replace(', , ', "")
    return val

def clean_text(val):
    stop = stopwords.words('english')
    val = misspelled_correction(val)
    val = ' '.join(val.lower().split())
    val = cont_to_meaning(val)
    val = p.clean(val)
    val = ' '.join(punctuation(emoji.demojize(val)).split())
    val = ' '.join(replace_special_punctua(val).split())
#     val = ' '.join([word for word in val.split() if word not in (stop)])
    return val

sent_to_id  = {"fa":0, "c":1,"start":2,"stub":3,"b":4, "ga":5}