import joblib
import nltk 
import pandas as pd
import numpy as np
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize 
stop_words = set(stopwords.words('english'))
import preprocessor as p
import emoji
from sklearn.model_selection import cross_val_score, GridSearchCV, KFold, RandomizedSearchCV, train_test_split
p.set_options(p.OPT.MENTION, p.OPT.URL)


misspell_data = pd.read_csv("data/aspell.txt",sep=":",names=["correction","misspell"])
misspell_data.misspell = misspell_data.misspell.str.strip()
misspell_data.misspell = misspell_data.misspell.str.split(" ")
misspell_data = misspell_data.explode("misspell").reset_index(drop=True)
misspell_data.drop_duplicates("misspell",inplace=True)
miss_corr = dict(zip(misspell_data.misspell, misspell_data.correction))

contractions = pd.read_csv("data/contractions.csv")
cont_dic = dict(zip(contractions.Contraction, contractions.Meaning))

def tokenize_text(text):
    tokens = []
    for sent in nltk.sent_tokenize(str(text)):
        if len(sent) < 2:
            continue
        for word in nltk.word_tokenize(sent):
            if word not in stop_words: 
                tokens.append(word.lower())
    return tokens
    
def CreateBalancedSampleWeights(y_train, largest_class_weight_coef):
    classes = np.unique(y_train, axis = 0)
    classes.sort()
    class_samples = np.bincount(y_train)
    total_samples = class_samples.sum()
    n_classes = len(class_samples)
    weights = total_samples / (n_classes * class_samples * 1.0)
    class_weight_dict = {key : value for (key, value) in zip(classes, weights)}
    class_weight_dict[classes[1]] = class_weight_dict[classes[1]] * largest_class_weight_coef
    sample_weights = [class_weight_dict[y] for y in y_train]
    return sample_weights, class_weight_dict
    
def misspelled_correction(val):
    for x in val.split(): 
        if x in miss_corr.keys(): 
            val = val.replace(x, miss_corr[x]) 
    return val
    
def cont_to_meaning(val): 
    for x in val.split(): 
        if x in cont_dic.keys(): 
            val = val.replace(x, cont_dic[x]) 
    return val
   
def punctuation(val): 
    punctuations = '''()-[]{};:'"\,<>./@#$%^&_~*.!!'''
    for x in val.lower(): 
        if x in punctuations: 
            val = val.replace(x, " ") 
    return val

def postprocess(df,result):
    df = pd.DataFrame(data = df)
    if df['lexicon_count'].values[0] >= 2347 or df['num_difficult_words'].values[0] >= 370:
        temp = result[0]
        result[0] = result.max()
        result[np.argmax(result)] = temp
    if 1100 <= df['lexicon_count'].values[0] <= 2000 and 370 >= df['num_difficult_words'].values[0] > 200:
        temp = result[5]
        result[5] = result.max()
        result[np.argmax(result)] = temp
    if 787 <= df['lexicon_count'].values[0] <= 1100 and 200 >= df['num_difficult_words'].values[0] > 150:
        temp = result[4]
        result[4] = result.max()
        result[np.argmax(result)] = temp
    if 500 < df['lexicon_count'].values[0] <= 787 and 150 >= df['num_difficult_words'].values[0] > 90:
        temp = result[1]
        result[1] = result.max()
        result[np.argmax(result)] = temp
    if 200 < df['lexicon_count'].values[0] <= 500 and 100 >= df['num_difficult_words'].values[0] > 40:
        temp = result[2]
        result[2] = result.max()
        result[np.argmax(result)] = temp
    if df['lexicon_count'].values[0] <= 200 or df['num_difficult_words'].values[0] <= 40:
        temp = result[3]
        result[3] = result.max()
        result[np.argmax(result)] = temp
    return result

def clean_text(val):
    val = misspelled_correction(val)
    val = cont_to_meaning(val)
    val = p.clean(val)
    val = ' '.join(punctuation(emoji.demojize(val)).split())
    val = ' '.join(val.lower().split())
    return val
    
